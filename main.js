const cinemaMode = document.querySelector("#cinema");
const videoScreen = document.querySelector("#videoScreen");
const cinemaBg = document.querySelector("#cinemaBg");
const playPause = document.querySelector("#playPause");
const videoBar = document.querySelector("#videoBar");
const playOnScreen = document.querySelector("#playOnScreen");
const h1 = document.querySelector("h1");
const fullScreen = document.querySelector("#fullScreen");
const description = document.querySelector("#description");
const showMore = document.querySelector("#showMore");
const hrs = document.querySelectorAll("hr");
const simulator = document.querySelector("#simulator");
const volumeControl = document.querySelector("#volumeControl");
const volumeValue = document.querySelector("#volumeValue");
const videoContainer = document.querySelector("#videoContainer");
const settings = document.querySelector("#settings");
const changeSpeed = document.querySelector("#changeSpeed");
const comments = document.querySelector("#comments");
const cancelBtn = document.querySelector("#cancelBtn");
const commentBtn = document.querySelector("#commentBtn");
const textarea = document.querySelector("textarea");

const speed = document.querySelector("#speedValue");

const playlist = document.querySelector("#playlist");

let simCount = 0;
let valueSpeed = 1;
let interv;
let timeout;
let volTime;
const howManyVideosOnPlaylist = 20;


let isCinemaMode = false;
let isPlaying = false;
let isFullScreen = false;
let scroll = false;
let isSettingsOn = false;

const getCinemaMode = () => {
    if(!isCinemaMode){
        videoScreen.style.width="1100px";
        videoScreen.style.height="620px";
        videoScreen.style.marginRight = "auto";
        videoScreen.style.marginLeft = "auto";

        videoContainer.style.width = "100%";
        description.style.marginRight = "auto";
        description.style.marginLeft = "auto";
        description.style.width="1100px";

        showMore.style.marginRight = "auto";
        showMore.style.marginLeft = "auto";
        showMore.style.width="1100px";

        title.style.marginRight = "auto";
        title.style.marginLeft = "auto";
        title.style.width="1100px";

        hrs.forEach((hr) => {
            hr.style.marginRight = "auto";
            hr.style.marginLeft = "auto";
            hr.style.width="1100px";
        });
        
        cinemaBg.style.backgroundColor  = "black";
        cinemaBg.style.width ="100%";

        playlist.style.float = "right";
        playlist.style.marginRight = "3%";
        playlist.style.marginTop = "1%";
        playlist.style.display = "none";

        comments.style.marginRight = "auto";
        comments.style.marginLeft = "auto";
        comments.style.width="1100px";

        if(isFullScreen){
            document.exitFullscreen();
        }
        
        isCinemaMode = !isCinemaMode;
    }else if(isCinemaMode){
        videoScreen.style.width="900px";
        videoScreen.style.height="500px";
        videoScreen.style.marginLeft = "5%";
        videoScreen.style.marginRight = "100px";

        videoContainer.style.width = "";
        description.style.width="900px";
        description.style.marginRight = "";
        description.style.marginLeft = "50px";

        title.style.width="900px";
        title.style.marginRight = "";
        title.style.marginLeft = "50px";

        showMore.style.width="900px";
        showMore.style.marginRight = "";
        showMore.style.marginLeft = "50px";

        hrs.forEach((hr) => {
            hr.style.width="900px";
            hr.style.marginRight = "";
            hr.style.marginLeft = "50px";
        });

        cinemaBg.style.backgroundColor  = "";
        cinemaBg.style.width ="";

        playlist.style.float = "";
        playlist.style.marginRight = "";
        playlist.style.marginTop = "";
        playlist.style.display = "block";

        comments.style.width="900px";
        comments.style.marginRight = "";
        comments.style.marginLeft = "50px";

        isCinemaMode = !isCinemaMode;
    }
};

const playPauseVideo = () => {
    if(!isPlaying){
        playPause.textContent = "PAUSE";
        interv = setInterval(() => {
            simulator.textContent = simCount++;
        }, 1000/valueSpeed)
        h1.textContent = "";
        isPlaying = !isPlaying;
    }else if(isPlaying){
        playPause.textContent = "PLAY"
        clearInterval(interv);
        h1.textContent = "PAUSE";

        isPlaying = !isPlaying;
    }
};

const playOnScreenVideo = () => {   
    if(!isPlaying){
        h1.textContent = "PLAY";
        
        timeout = setTimeout(() => {
            h1.textContent = "";
        }, 3000);
        
        playPause.textContent = "PAUSE";
        
        interv = setInterval(() => {
            simulator.textContent = simCount++;
        }, 1000/valueSpeed)
        
        isPlaying = !isPlaying;
    }else if(isPlaying){
        h1.textContent = "PAUSE";
        playPause.textContent = "PLAY";
        
        clearTimeout(timeout);
        clearInterval(interv);
        
        isPlaying = !isPlaying;
    }
};

const showVideoBar = () => {
    videoBar.style.display = "block";
    playPause.style.display = "block";
    cinemaMode.style.display = "block";
    playOnScreen.style.height = `${videoScreen.offsetHeight}px`;
    playOnScreen.style.width = `${videoScreen.offsetWidth}px`;
};

const hideVideoBar = () => {
    videoBar.style.display = "none";
    playPause.style.display = "none";
    cinemaMode.style.display = "none";
    
    if(isPlaying){
        h1.textContent = "";
    }else if(!isPlaying){
        h1.textContent = "PAUSE";
    }

    playOnScreen.style.height = `${videoScreen.offsetHeight}px`;
    playOnScreen.style.width = `${videoScreen.offsetWidth}px`;
};

const getFullScreen = () => {
    if(!isFullScreen){
        videoScreen.requestFullscreen().catch((e) => {
            console.log(e);
        })
        isFullScreen = !isFullScreen;
    }else if(isFullScreen){
        document.exitFullscreen();
        
        isFullScreen = !isFullScreen;
    }
}

const changeVideoSpeed = () => {
    if(isSettingsOn){
        changeSpeed.style.display = "none";
        isSettingsOn = !isSettingsOn;
    }else if(!isSettingsOn) {
        changeSpeed.style.display = "block";
        isSettingsOn = !isSettingsOn;
    }
}

description.textContent = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`;

showMore.addEventListener("click", () => {
    if(scroll){
        description.style.height = "40px";
        scroll = !scroll;
        showMore.textContent="Show more";
    }else if(!scroll){
        description.style.height = "";
        scroll = !scroll;
        showMore.textContent='Show less';
    }
})

volumeControl.addEventListener("input", (e) => {
    volumeValue.textContent = e.currentTarget.value;
    
    clearTimeout(volTime);
    
    volTime = setTimeout(() => {
        volumeValue.textContent = "";
    }, 3000);
});

document.forms["changeSpeedForm"].addEventListener("click", (e) => {
    valueSpeed = document.forms["changeSpeedForm"]["speed"].value;
    speed.textContent=valueSpeed;
    if(isPlaying){
        clearInterval(interv);
        interv = setInterval(() => {
            simulator.textContent = simCount++;
        }, 1000/valueSpeed);
    }
})

const addVideosToPlaylist = () => {
    for(let i = 0; i < howManyVideosOnPlaylist; i++){
        const div = document.createElement("div");
        div.classList.add("playlistVideo");
        div.textContent = i+1;

        playlist.appendChild(div);
    }
};

addVideosToPlaylist();

cinemaMode.addEventListener("click",getCinemaMode);
fullScreen.addEventListener("click", getFullScreen);

playPause.addEventListener("click", playPauseVideo);
playOnScreen.addEventListener("click", playOnScreenVideo);

document.addEventListener("keydown", (e) => {
    if(textarea != document.activeElement){
        if(e.keyCode  == 32){
            playOnScreenVideo();
        } else if(e.keyCode == 39){
            simCount += 5;
        }else if(e.keyCode == 37){
            (simCount <= 0) ? simCount = 0 : simCount -= 5;
        }
    } 
});

videoScreen.addEventListener("mouseover", showVideoBar);
playOnScreen.addEventListener("dblclick", getFullScreen);
videoScreen.addEventListener("mouseout", hideVideoBar);

settings.addEventListener("click", changeVideoSpeed);

textarea.addEventListener("click", (e) => {
    e.preventDefault();
    cancelBtn.style.display = "block";
    cancelBtn.style.float = "left";
    commentBtn.style.display = "block";
    commentBtn.style.float = "left";
})

cancelBtn.addEventListener("click", (e) => {
    e.preventDefault();
    cancelBtn.style.display = "none";
    commentBtn.style.display = "none";
});

commentBtn.addEventListener("click", (e) => {
    e.preventDefault();
    if(textarea.value){
        console.log(textarea.value);
        const div = document.createElement("span");
        div.textContent = textarea.value;
        div.classList.add("comment");

        document.querySelector("#allComments").prepend(div);
    }
    textarea.value = "";
    cancelBtn.style.display = "none";
    commentBtn.style.display = "none";
});